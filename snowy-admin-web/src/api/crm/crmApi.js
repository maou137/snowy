/**
 *  Copyright [2022] [https://www.xiaonuo.vip]
 *	Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *	1.请不要删除和修改根目录下的LICENSE文件。
 *	2.请不要删除和修改Snowy源码头部的版权声明。
 *	3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 *	4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 *	5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 *	6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
import { baseRequest } from '@/utils/bizRequest'

const request = (url, ...arg) => baseRequest(`/crm/${url}`, ...arg)

const wechatRequest = (url, ...arg) => baseRequest(`/wxopen/${url}`, ...arg)
/**
 * 单页
 *
 * @author yubaoshan
 * @date 2022-09-22 22:33:20
 */
export default {
	exportExcel(url, fileName, data) {
		return request(url, data, 'post', {
			responseType: 'blob'
		}).then(res => {
			const a = document.createElement('a')
			const href = URL.createObjectURL(res.data)
			a.download = `${fileName}.xlsx`
			a.href = href
			a.click()
			URL.revokeObjectURL(href)
		})
	},
	sendMesage(data) {
		return wechatRequest('message/send', data)
	},
	cardSaleOrderItemPage(data) {
		return request('cardSaleOrder/items', data, 'get')
	},
	cardSaleOrderCancel(data) {
		return request('cardSaleOrder/cancel', data)
	},
	voucherOrderSubmitForm(data, edit = false) {
		return request(edit ? 'voucherOrder/update' : 'voucherOrder/create', data)
	},
	voucherOrderRedo(data) {
		return request('voucherOrder/redo', data)
	},
	activateCard(data) {
		return request('card/activate', data)
	},
	bonusConfigSubmitForm(data, edit = false) {
		return request(edit ? 'bonusConfig/update' : 'bonusConfig/create', data)
	},
	bonusPayList(data) {
		return request('bonusPay/list', data, 'get')
	},
	cardPayPage(data) {
		return request('cardPay/list', data, 'get')
	},
	voucherRuleSubmitForm(data, edit = false) {
		return request(edit ? 'voucherRule/update' : 'voucherRule/create', data)
	},
	voucherOrderPage(data) {
		return request('voucherOrder/list', data, 'get')
	},
	voucherRulePage(data) {
		return request('voucherRule/list', data, 'get')
	},
	voucherRuleStop(data) {
		return request('voucherRule/stop', data)
	},
	voucherRuleEnable(data) {
		return request('voucherRule/enable', data)
	},
	// 获取卡类型分页
	cardTypePage(data) {
		return request('cardType/list', data, 'get')
	},
	cardTypeStock(data) {
		return request('cardTypeStock/list', data, 'get')
	},
	cardOrderPage(data) {
		return request('cardOrder/list', data, 'get')
	},
	cardSaleOrderPage(data) {
		return request('cardSaleOrder/list', data, 'get')
	},
	cardOrderAudit(data) {
		return request('cardOrder/audit', data)
	},
	memberDelete(data) {
		return request('memberCard/delete', data)
	},
	couponPage(data) {
		return request('coupon/list', data, 'get')
	},
	couponItemPage(data) {
		return request('couponItem/list', data, 'get')
	},
	adjustAccountPage(data) {
		return request('adjustAccount/list', data, 'get')
	},
	memberAdjustAccountPage(data) {
		return request('member/adjustAccount/list', data, 'get')
	},
	cardOrderSubmitForm(data, edit = false) {
		return request(edit ? 'cardOrder/update' : 'cardOrder/create', data)
	},
	setMemberRankSubmitForm(data) {
		return request('memberCard/setMemberRank', data)
	},
	cardSaleOrderSubmitForm(data, edit = false) {
		return request(edit ? 'cardSaleOrder/update' : 'cardSaleOrder/create', data)
	},
	couponActivitySubmitForm(data, edit = false) {
		return request(edit ? 'coupon/update' : 'coupon/create', data)
	},
	adjustAccountSubmitForm(data) {
		return request('card/adjustAccount', data)
	},
	adjustMemberAccountSubmitForm(data) {
		return request('memberCard/adjustAccount', data)
	},
	adjustMemberBonusSubmitForm(data) {
		return request('bonus/adjust', data)
	},
	removeCardType(data) {
		return request('cardType/delete', data)
	},
	voucherRuleRemove(data) {
		return request('voucherRule/delete', data)
	},
	cardTypeSubmitForm(data, edit = false) {
		return request(edit ? 'cardType/update' : 'cardType/create', data)
	},
	memberCardSubmitForm(data, edit = false) {
		return request(edit ? 'memberCard/update' : 'memberCard/create', data)
	},
	cardSubmitForm(data, edit = false) {
		return request(edit ? 'card/update' : 'card/create', data)
	},
	memberRankSubmitForm(data, edit = false) {
		return request(edit ? 'memberRank/update' : 'memberRank/create', data)
	},
	cardLogList(data) {
		return request('cardLog/list', data, 'get')
	},
	bonusLogList(data) {
		return request('bonusLog/list', data, 'get')
	},
	bonusRuleList(data) {
		return request('bonusRule/list', data, 'get')
	},
	memberLogList(data) {
		return request('memberLog/list', data, 'get')
	},
	memberRankLogList(data) {
		return request('memberRankLog/list', data, 'get')
	},
	memberRankDelete(data) {
		return request('memberRank/delete', data)
	},
	voucherOrderRemove(data) {
		return request('voucherOrder/delete', data)
	},
	memberPage(data) {
		return request('member/page', data, 'get')
	},
	memberRankPage(data) {
		return request('memberRank/list', data, 'get')
	},
	cardDelete(data) {
		return request('card/delete', data)
	},
	couponDelete(data) {
		return request('coupon/delete', data)
	},
	cardStop(data) {
		return request('card/stop', data)
	},
	cardEnable(data) {
		return request('card/enable', data)
	},
	cardPage(data) {
		return request('card/list', data, 'get')
	},
	cancelCard(data) {
		return request('card/cancel', data)
	}
}
