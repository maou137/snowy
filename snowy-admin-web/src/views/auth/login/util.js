import loginApi from '@/api/auth/loginApi'
import userCenterApi from '@/api/sys/userCenterApi'
import dictApi from '@/api/dev/dictApi'
import crmApi from "@/api/crm/crmApi"
import bizOrgApi from "@/api/biz/bizOrgApi"
import orgApi from '@/api/sys/orgApi'
import router from '@/router'
import tool from '@/utils/tool'
import { message } from 'ant-design-vue'
import { useGlobalStore } from '@/store'

export const afterLogin = async (loginToken) => {
	tool.data.set('TOKEN', loginToken)
	// 获取登录的用户信息
	const loginUser = await loginApi.getLoginUser()
	const globalStore = useGlobalStore()
	globalStore.setUserInfo(loginUser)
	tool.data.set('USER_INFO', loginUser)

	// 获取用户的菜单
	const menu = await userCenterApi.userLoginMenu()
	const indexMenu = menu[0].children[0].path
	tool.data.set('MENU', menu)
	// 重置系统默认应用
	tool.data.set('SNOWY_MENU_MODULE_ID', menu[0].id)
	message.success('登录成功')
	router.replace({
		path: indexMenu
	})
	dictApi.dictTree().then((data) => {
		// 设置字典到store中
		tool.data.set('DICT_TYPE_TREE_DATA', data)
	})
	crmApi.voucherRulePage({ pageSize: 100, size: 100, storeCode: '0000' }).then(data => {
		tool.data.set('CRM_VOUCHER_RULE', data.records || data)
	})
	crmApi.cardTypePage({ pageSize: 100 }).then(data => {
		tool.data.set('CRM_CARD_TYPE', data.records || data)
	})
	crmApi.memberRankPage({ pageSize: 100 }).then(data => {
		tool.data.set('CRM_MEMBER_RANK', data.records || data)
	})
	bizOrgApi.orgPage({ pageSize: 1000, size: 1000 }).then(data => {
		let records = data.records || data
		// records = records.map(item => {
		// 	return {
		// 		id: item.code,
		// 		name: item.name
		// 	}
		// })
		tool.data.set('ORG_CATEGORY', records)
	})
}
